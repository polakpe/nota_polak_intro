local sensorInfo = {
	name = "getUnitType",
	desc = "Return data of actual wind.",
	author = "Peter",
	date = "2019-04-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


function list_iter (t)
	local i = 0
	local n = table.getn(t)
	return function ()
			 i = i + 1
			 if i <= n then return t[i] end
		   end
  end

return function(x, y, z, t)
	local armbox = {}
	local atlas = {}
	local others = {}

	local MyUnits = Spring.GetSelectedUnits()
  local itu = list_iter(MyUnits)
	while true do
		local unit = itu()
		if unit == nil then break end

		if UnitDefs[Spring.GetUnitDefID(unit)].name == "armmllt" then
			table.insert( armbox, unit )

		elseif UnitDefs[Spring.GetUnitDefID(unit)].name == "armatlas" then
			table.insert( atlas, unit )
		
		elseif UnitDefs[Spring.GetUnitDefID(unit)].name == "armbox" then
			table.insert( armbox, unit )

		elseif UnitDefs[Spring.GetUnitDefID(unit)].name == "armham" then
			table.insert( others, unit )
		end

		

	end

	local alt = list_iter(atlas)
	local box = list_iter(armbox)

	while true do
		local a = alt()
		if a == nil then break end
		
		local b = box()
		if b == nil then break end
		Spring.GiveOrderToUnit(a,
      CMD.INSERT,
      {-1,CMD.MOVE,CMD.OPT_SHIFT,t.x,t.y,t.z},
      {"alt"}
		)
		Spring.GiveOrderToUnit(a,
      CMD.INSERT,
      {-1,CMD.LOAD_UNITS,CMD.OPT_SHIFT,b},
      {"alt"}
		)
		Spring.GiveOrderToUnit(a,
      CMD.INSERT,
      {-1,CMD.MOVE,CMD.OPT_SHIFT,t.x,t.y,t.z},
      {"alt"}
		)
		Spring.GiveOrderToUnit(a,
      CMD.INSERT,
      {-1,CMD.MOVE,CMD.OPT_SHIFT,x,y,z},
      {"alt"}
		)
		--[[
		Spring.GiveOrderToUnit(a, CMD.MOVE,{t.x,t.y,t.z},{"shift"})
		Spring.GiveOrderToUnit(a, CMD.LOAD_UNITS,{b},{"shift"})
		Spring.GiveOrderToUnit(a, CMD.MOVE,{x,y,z},{"shift"})
		--]]
		
	end

	local box = list_iter(others)
	local b = 0
	local a = 0
	while true do
		b = box()
		if b == nil then break end

		a = alt()
		if a == nil then 
			Spring.Echo("s : ", a)
			Spring.GiveOrderToUnit(b, CMD.MOVE,{x,y,z},{"shift"})
		else

		
			Spring.GiveOrderToUnit(a, CMD.LOAD_UNITS,{b},{"shift"})
			Spring.GiveOrderToUnit(a, CMD.MOVE,{x,y,z},{"shift"})
		end
	end

		return atlas
end
