local sensorInfo = {
	name = "findHills",
	desc = "Return data of actual wind.",
	author = "Peter",
	date = "2019-04-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


function list_iter (t)
	local i = 0
	local n = table.getn(t)
	return function ()
			 i = i + 1
			 if i <= n then return t[i] end
		   end
  end

function findHills()
	local max = 0
	local hills = {}
	for x = 0,0+Game.mapSizeX,10 do
		for z = 0,0+Game.mapSizeZ,10 do
			local y = Spring.GetGroundHeight(x,z)
			if y > max then 
				max = y
			end
		end
	end

	for x = 0,0+Game.mapSizeX,10 do
		for z = 0,0+Game.mapSizeZ,10 do
			local y = Spring.GetGroundHeight(x,z)
			if y == max then 
				local i = true
				for ii,h in ipairs(hills) do
					if h:Distance(Vec3(x,y,z)) < 300 then
						i = false
						break
					end
				end
				if i then
					table.insert( hills,Vec3(x,y,z))
				end
			end
		end
	end
	return hills
end

return function()
	local Hills = findHills()
	local MyUnits = Spring.GetSelectedUnits()
    itu = list_iter(MyUnits)
	ith = list_iter(Hills)
	while true do
		local unit = itu()
		if unit == nil then break end
		local hill = ith()
		if hill == nil then break end
		if UnitDefs[Spring.GetUnitDefID(unit)].name ~= "armthovr" then
			Spring.GiveOrderToUnit(unit, CMD.MOVE,hill:AsSpringVector(), {})
		end

	end
end
